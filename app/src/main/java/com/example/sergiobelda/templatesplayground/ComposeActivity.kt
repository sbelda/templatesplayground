package com.example.sergiobelda.templatesplayground

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sergiobelda.templatesplayground.databinding.ActivityComposeBinding

class ComposeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityComposeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityComposeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
        binding.composeView.setContent {
            TemplatesPlaygroundTheme {
                LazyColumn {
                    items(products) {
                        when (it.template) {
                            ONEA -> mapTemplate(product = it, component = template1A.component)
                            ONEB -> mapTemplate(product = it, component = template1B.component)
                        }
                        Divider(
                            thickness = 1.dp,
                            modifier = Modifier.padding(top = 4.dp, bottom = 4.dp)
                        )
                    }
                }
            }
        }

         */
    }
}
