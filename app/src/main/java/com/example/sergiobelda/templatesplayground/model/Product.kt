package com.example.sergiobelda.templatesplayground.model

/*
class Product : BaseObservable() {

    @get:Bindable
    var name: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.productName)
        }
}
*/

data class Product(
    val name: String = "",
    val price: String = "",
    val description: String = "",
    val url: String = ""
)

val product = Product(
    "Product",
    "100€",
    "Text",
    "https://i.picsum.photos/id/964/1080/600.jpg?hmac=8vc4FBRvNr--ELpzCY15Ij8SkCu6qv1ONks2FgFrBZU"
)

val product2 = Product(
    "Product 2",
    "12.23€",
    "Text",
    "https://i.picsum.photos/id/296/1080/600.jpg?hmac=FZZkA6UxCg7rv7A1VVpP_si7cfa0_dyJh1zlxiscI-g"
)

val product3 = Product(
    "Product 3",
    "30.40€",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec diam suscipit, semper magna in, sollicitudin massa. Suspendisse in consequat diam. Aliquam et sollicitudin ante. Mauris dictum finibus magna. Nunc turpis lorem, feugiat ac elit tincidunt, aliquet convallis metus. Nulla nec nulla ex. Donec lobortis leo a tincidunt vulputate.\n" +
        "\n" +
        "Proin nec dui luctus, viverra ligula non, consectetur enim. Aliquam eu enim lacus. Vestibulum mollis, sapien quis tristique commodo, lacus augue tempus elit, sit amet tristique nisl risus ac enim. Vivamus sollicitudin consectetur mauris, condimentum facilisis ex condimentum vel. Curabitur et enim neque. Nunc porta nulla vitae neque condimentum porttitor. Nullam lobortis rhoncus metus, pulvinar luctus elit posuere quis. Nam quis vehicula mauris, aliquet tincidunt dui. Curabitur non posuere enim. Nam placerat, augue ac tincidunt malesuada, turpis ante sodales nisi, in lobortis ex mauris non dui. Suspendisse a maximus est.\n" +
        "\n" +
        "Nunc quis eros eu sem fringilla eleifend. Cras id dapibus sapien. Integer ante elit, dictum sit amet justo nec, maximus elementum risus. Morbi sit amet ornare diam, ut porta lectus. Fusce commodo varius turpis, vel mollis nisi tincidunt sit amet. Nulla ut rutrum massa. Sed eget lectus euismod, ornare lorem id, cursus nulla. Vestibulum convallis, nibh quis rutrum fermentum, nulla libero euismod urna, in imperdiet felis est imperdiet augue. Fusce posuere ex ut ligula ultricies laoreet. Donec dictum lacus quis fringilla fermentum.\n" +
        "\n" +
        "Vestibulum vel neque magna. Etiam convallis non dui et porta. Aliquam tincidunt viverra ipsum ac rhoncus. In placerat quam libero, ac ultrices ipsum fermentum sed. Aliquam semper nisi mollis fermentum scelerisque. Pellentesque non rhoncus libero. Nulla et tincidunt purus. Duis scelerisque libero felis, non volutpat ex ornare laoreet. In hac habitasse platea dictumst. Donec ac placerat metus. Nullam quis ligula ex. Nam in orci eros.",
    "https://i.picsum.photos/id/313/1080/600.jpg?hmac=WI3xsSD1qOEpT9ESwf7CWn4p4q8Au7jmgdbfzdu67IM"
)

val productBlock1 = ProductsBlock(ONEA).apply { insert(product) }

val productBlock2 = ProductsBlock(TWOA).apply { insert(product, product2) }

val productBlock3 = ProductsBlock(THREEA).apply { insert(product, product2, product3) }

data class ProductsBlock(
    val templateCode: String = "",
    val list: ArrayList<Product> = arrayListOf()
) {

    fun insert(vararg products: Product) {
        list.addAll(products)
    }
}
