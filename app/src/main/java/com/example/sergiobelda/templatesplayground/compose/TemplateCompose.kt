package com.example.sergiobelda.templatesplayground.compose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.sergiobelda.templatesplayground.model.Component
import com.example.sergiobelda.templatesplayground.model.Product
import dev.chrisbanes.accompanist.coil.CoilImage

@Composable
fun mapTemplate(product: Product, component: Component) {
    when (component) {
        is Component.Column -> Column(
            modifier = Modifier.padding(
                PaddingValues(
                    start = component.style.paddingStart.dp,
                    end = component.style.paddingEnd.dp,
                    top = component.style.paddingTop.dp,
                    bottom = component.style.paddingBottom.dp
                )
            )
        ) {
            component.children.forEach {
                mapTemplate(product, component = it)
            }
        }
        is Component.Row -> Row(
            modifier = Modifier.padding(
                PaddingValues(
                    start = component.style.paddingStart.dp,
                    end = component.style.paddingEnd.dp,
                    top = component.style.paddingTop.dp,
                    bottom = component.style.paddingBottom.dp
                )
            )
        ) {
            component.children.forEach {
                mapTemplate(product, component = it)
            }
        }
        is Component.Text -> Text(
            text = "Text",
            modifier = Modifier.padding(
                PaddingValues(
                    start = component.textStyle.paddingStart.dp,
                    end = component.textStyle.paddingEnd.dp,
                    top = component.textStyle.paddingTop.dp,
                    bottom = component.textStyle.paddingBottom.dp
                )
            )
        )
        is Component.ProductExtendedInfo -> ProductExtendedInfo(product)
        is Component.ProductInfo -> ProductInfo(product)
        is Component.ProductMedia -> ProductMedia(product = product)
    }
}

@Composable
fun ProductMedia(product: Product) {
    CoilImage(data = product.url, contentDescription = "")
}

@Composable
fun ProductInfo(product: Product) {
    Column {
        Text(text = product.name)
        Text(text = product.price)
    }
}

@Composable
fun ProductExtendedInfo(product: Product) {
    Column {
        Text(text = product.name)
        Text(text = product.description)
        Text(text = product.price)
    }
}
