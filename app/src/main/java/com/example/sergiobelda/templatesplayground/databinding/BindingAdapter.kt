package com.example.sergiobelda.templatesplayground.databinding

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import coil.load
import com.example.sergiobelda.templatesplayground.model.Product

@BindingAdapter("imageUrl")
fun imageUrl(imageView: ImageView, url: String?) {
    imageView.load(url)
}

// IGNORE
@BindingAdapter("productName")
fun productName(textView: TextView, product: Product) {
    textView.text = product.name
}
