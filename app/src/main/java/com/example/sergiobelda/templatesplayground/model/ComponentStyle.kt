package com.example.sergiobelda.templatesplayground.model

import androidx.annotation.FloatRange

open class Style(
    open var padding: Int = 0,
    open var paddingStart: Int = padding,
    open var paddingEnd: Int = padding,
    open var paddingTop: Int = padding,
    open var paddingBottom: Int = padding,
    @FloatRange(from = 0.0, to = 1.0) open var startPercentage: Float = 0f,
    @FloatRange(from = 0.0, to = 1.0) open var endPercentage: Float = MATCH_PARENT
)

fun style(block: Style.() -> Unit): Style = Style().apply(block)

class TextStyle(
    override var padding: Int = 0,
    override var paddingStart: Int = padding,
    override var paddingEnd: Int = padding,
    override var paddingTop: Int = padding,
    override var paddingBottom: Int = padding,
    var textSize: Float = 12f
) : Style(padding, paddingStart, paddingEnd, paddingTop, paddingBottom, 0f, MATCH_PARENT)

fun textStyle(block: TextStyle.() -> Unit): TextStyle = TextStyle().apply(block)

const val MATCH_PARENT = 1.0f
