package com.example.sergiobelda.templatesplayground.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import com.example.sergiobelda.templatesplayground.databinding.ProductMediaViewBinding
import com.example.sergiobelda.templatesplayground.model.Product

class ProductMediaView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ProductView(context, attrs, defStyleAttr) {

    private val binding = ProductMediaViewBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )

    override fun setProduct(product: Product) {
        binding.product = product
    }
}
