package com.example.sergiobelda.templatesplayground.model

sealed class Component(open var style: Style = Style()) {
    data class Column(var children: Children = Children(), override var style: Style = Style()) : Component(style)
    data class Row(var children: Children = Children(), override var style: Style = Style()) : Component(style)

    @Deprecated("Not Use Text")
    data class Text(var id: Int = 1, var textStyle: TextStyle = TextStyle()) : Component(textStyle)

    data class ProductMedia(
        val position: Int,
        override var style: Style = Style()
    ) : Component(style)

    data class ProductInfo(
        val position: Int,
        override var style: Style = Style(),
        var nameStyle: TextStyle = TextStyle(),
        var priceStyle: TextStyle = TextStyle()
    ) : Component(style)

    data class ProductExtendedInfo(
        val position: Int,
        override var style: Style = Style(),
        var nameStyle: TextStyle = TextStyle(),
        var priceStyle: TextStyle = TextStyle(),
        var descriptionStyle: TextStyle = TextStyle()
    ) : Component(style)
}

class Children : ArrayList<Component>() {
    operator fun Component.unaryPlus() {
        add(this)
    }
}
