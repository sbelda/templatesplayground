package com.example.sergiobelda.templatesplayground.customview

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.example.sergiobelda.templatesplayground.model.Product

abstract class ProductView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        this.orientation = VERTICAL
    }

    abstract fun setProduct(product: Product)
}
