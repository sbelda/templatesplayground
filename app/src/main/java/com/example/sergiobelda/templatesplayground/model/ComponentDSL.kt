package com.example.sergiobelda.templatesplayground.model

fun Column(
    style: Style.() -> Unit = { Style() },
    children: Children.() -> Unit = { Children() }
): Component.Column =
    Component.Column(
        style = Style().apply(style),
        children = Children().apply(children)
    )

fun Row(
    style: Style.() -> Unit = { Style() },
    children: Children.() -> Unit = { Children() }
): Component.Row =
    Component.Row(
        style = Style().apply(style),
        children = Children().apply(children)
    )

fun ProductMedia(
    position: Int,
    style: Style.() -> Unit = { Style() }
): Component.ProductMedia =
    Component.ProductMedia(
        position = position,
        style = Style().apply(style)
    )

fun ProductInfo(
    position: Int,
    style: Style.() -> Unit = { Style() },
    nameStyle: TextStyle.() -> Unit = { TextStyle() },
    priceStyle: TextStyle.() -> Unit = { TextStyle() }
): Component.ProductInfo =
    Component.ProductInfo(
        position = position,
        style = Style().apply(style),
        nameStyle = TextStyle().apply(nameStyle),
        priceStyle = TextStyle().apply(priceStyle)
    )

fun ProductExtendedInfo(
    position: Int,
    style: Style.() -> Unit = { Style() },
    nameStyle: TextStyle.() -> Unit = { TextStyle() },
    priceStyle: TextStyle.() -> Unit = { TextStyle() },
    descriptionStyle: TextStyle.() -> Unit = { TextStyle() }
): Component.ProductExtendedInfo =
    Component.ProductExtendedInfo(
        position = position,
        style = Style().apply(style),
        nameStyle = TextStyle().apply(nameStyle),
        priceStyle = TextStyle().apply(priceStyle),
        descriptionStyle = TextStyle().apply(descriptionStyle)
    )
