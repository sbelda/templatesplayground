package com.example.sergiobelda.templatesplayground

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.Guideline
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.example.sergiobelda.templatesplayground.customview.ProductExtendedInfoView
import com.example.sergiobelda.templatesplayground.customview.ProductInfoView
import com.example.sergiobelda.templatesplayground.customview.ProductMediaView
import com.example.sergiobelda.templatesplayground.customview.ProductView
import com.example.sergiobelda.templatesplayground.databinding.TemplateViewBinding
import com.example.sergiobelda.templatesplayground.model.Component
import com.example.sergiobelda.templatesplayground.model.Product
import com.example.sergiobelda.templatesplayground.model.ProductsBlock
import com.example.sergiobelda.templatesplayground.model.Template

/**
 * Empty Template Container.
 */
class TemplateView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private var _templateCode: String? = null
    val templateCode: String? get() = _templateCode

    private val product: MutableLiveData<Product> = MutableLiveData()

    private val productsBlock: MutableLiveData<ProductsBlock> = MutableLiveData()

    private val productsIdsMap = mutableMapOf<Int, Int>()

    private var binding: TemplateViewBinding = TemplateViewBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )

    fun setLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        product.observe(
            lifecycleOwner,
            {
                // Idea: Maybe use a Map<K, V> here?
                // findViewById<TextView>(it.templateNameId)?.text = it.name
                // findViewById<TextView>(it.templatePriceId)?.text = it.price
                // Alternative using specific type of content and predefined IDs.
                // findViewById<TextView>(R.id.product_name_text)?.text = it.name
                findViewById<ProductMediaView>(R.id.product_media_view)?.setProduct(it)
                findViewById<ProductInfoView>(R.id.product_info_view)?.setProduct(it)
                findViewById<ProductExtendedInfoView>(R.id.product_extended_info_view)?.setProduct(it)
            }
        )
        productsBlock.observe(
            lifecycleOwner,
            { productsBlock ->
                productsIdsMap.forEach { (key, value) ->
                    productsBlock.list.getOrNull(value)?.let {
                        findViewById<ProductView>(key).setProduct(it)
                    }
                }
            }
        )
    }

    fun setProductBlock(productsBlock: ProductsBlock) {
        this.productsBlock.value = productsBlock
    }

    fun setProduct(product: Product) {
        this.product.value = product
    }

    fun setTemplate(template: Template) {
        _templateCode = template.code
        setComponent(template.component)
    }

    private fun setComponent(component: Component) {
        binding.templateContainer.addView(component.mapTemplate(context))
    }

    private fun Component.mapTemplate(context: Context): View {
        // Should remove previous content
        return when (this) {
            is Component.Column -> {
                val view = this.map(context)
                this.children.forEach {
                    view.addView(it.mapTemplate(context))
                }
                view
            }
            is Component.Row -> {
                val view = this.map(context)
                this.children.forEach {
                    val childView = it.mapTemplate(context)
                    childView.id = generateViewId()
                    childView.layoutParams.width = 0
                    // childView.layoutParams.height = 0
                    view.addView(childView)

                    val startGuideline = createGuideline(context, ConstraintLayout.LayoutParams.VERTICAL)
                    startGuideline.setGuidelinePercent(it.style.startPercentage)
                    view.addView(startGuideline)

                    val endGuideline = createGuideline(context, ConstraintLayout.LayoutParams.VERTICAL)
                    endGuideline.setGuidelinePercent(it.style.endPercentage)
                    view.addView(endGuideline)

                    val constraintSet = ConstraintSet()
                    constraintSet.clone(view)

                    constraintSet.connect(childView.id, ConstraintSet.START, startGuideline.id, ConstraintSet.START)
                    constraintSet.connect(childView.id, ConstraintSet.END, endGuideline.id, ConstraintSet.END)
                    // constraintSet.connect(childView.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
                    // constraintSet.connect(childView.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                    constraintSet.applyTo(view)
                }
                view
            }
            is Component.Text -> this.map(context)
            // Alternative using specific type of content and predefined IDs.
            /*
            is ProductName -> TextView(context).also {
                // res/values/ids
                it.id = R.id.product_name_text
            }
             */
            // Alternative using DataBinding and specific type of content -> Check if there's any
            // way to simplify this:
            // https://developer.android.com/topic/libraries/data-binding/observability
            /*
            is ProductName -> TextView(context).also {

                binding.addOnPropertyChangedCallback(object :
                    Observable.OnPropertyChangedCallback() {
                    override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                        if (propertyId == BR.productName) {
                            it.text = binding.productName
                        }
                    }
                })
            }
            */
            is Component.ProductExtendedInfo -> this.map(context)
            is Component.ProductInfo -> this.map(context)
            is Component.ProductMedia -> this.map(context)
        }
    }

    private fun Component.Column.map(context: Context) = LinearLayout(context).also {
        it.orientation = LinearLayout.VERTICAL
        it.setPadding(
            this.style.paddingStart,
            this.style.paddingTop,
            this.style.paddingEnd,
            this.style.paddingBottom
        )
        it.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
    }

    private fun Component.Row.map(context: Context) = ConstraintLayout(context).also {
        it.setPadding(
            this.style.paddingStart,
            this.style.paddingTop,
            this.style.paddingEnd,
            this.style.paddingBottom
        )
        // TODO: 22/03/2021 FIX NOT SETTING A VALUE FOR HEIGHT
        it.layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            800
        )
    }

    // Problem here: Id already exists?
    private fun Component.Text.map(context: Context) = TextView(context).also {
        it.id = this.id
        it.setPadding(
            this.textStyle.paddingStart,
            this.textStyle.paddingTop,
            this.textStyle.paddingEnd,
            this.textStyle.paddingBottom
        )
    }

    private fun Component.ProductMedia.map(context: Context) = ProductMediaView(context).also {
        val id = generateViewId()
        it.id = id
        productsIdsMap[id] = position
    }

    private fun Component.ProductInfo.map(context: Context) = ProductInfoView(context).also {
        val id = generateViewId()
        it.id = id
        productsIdsMap[id] = position

        it.setProductNameStyle(nameStyle)
        it.setProductPriceStyle(priceStyle)
    }

    private fun Component.ProductExtendedInfo.map(context: Context) = ProductExtendedInfoView(context).also {
        val id = generateViewId()
        it.id = id
        productsIdsMap[id] = position

        it.setProductNameStyle(nameStyle)
        it.setProductDescriptionStyle(descriptionStyle)
        it.setProductPriceStyle(priceStyle)
    }

    private fun createGuideline(context: Context, orientation: Int): Guideline {
        val guideline = Guideline(context)
        guideline.id = Guideline.generateViewId()
        val layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.WRAP_CONTENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.orientation = orientation
        layoutParams.validate()
        guideline.layoutParams = layoutParams
        return guideline
    }
}
