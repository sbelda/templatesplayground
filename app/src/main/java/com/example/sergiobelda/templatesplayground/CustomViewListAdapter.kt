package com.example.sergiobelda.templatesplayground

import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.sergiobelda.templatesplayground.model.Product

class CustomViewListAdapter(val items: ArrayList<Product>, val templates: List<TemplateView>) :
    RecyclerView.Adapter<CustomViewListAdapter.ViewHolder>() {

    inner class ViewHolder(val view: FrameLayout) : RecyclerView.ViewHolder(view) {

        fun bind(product: Product) {
            /*
            templates.firstOrNull { it.templateCode == product.template }?.let {
                view.addView(it)
                it.setProduct(product)
            }

             */
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            FrameLayout(
                parent.context
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items.getOrNull(position)?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int = items.size
}
