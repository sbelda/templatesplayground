package com.example.sergiobelda.templatesplayground.model

data class Template(
    val code: String,
    val component: Component
)

fun Template(code: String, component: () -> Component): Template = Template(code, component())

const val ONEA = "1A"
const val TWOA = "2A"
const val ONEB = "1B"
const val THREEA = "3A"

val template1A = Template(
    code = ONEA
) {
    Column {
        +ProductMedia(
            position = 0,
        )
        +ProductInfo(
            position = 0,
            nameStyle = {
                textSize = 14f
            },
            priceStyle = {
                textSize = 12f
            }
        )
    }
}

val template2A = Template(
    code = TWOA
) {
    Column {
        +ProductMedia(
            position = 0
        )
        +ProductInfo(
            position = 0,
            nameStyle = {
                textSize = 14f
            },
            priceStyle = {
                textSize = 12f
            }
        )
        +ProductMedia(
            position = 1
        )
        +ProductInfo(
            position = 1,
            nameStyle = {
                textSize = 20f
            },
            priceStyle = {
                textSize = 16f
            }
        )
    }
}

val template1B = Template(
    code = ONEB
) {
    Column {
        +ProductExtendedInfo(
            position = 0,
            style = {
            },
            nameStyle = {
                textSize = 14f
            },
            descriptionStyle = {
            },
            priceStyle = {
                textSize = 12f
            }
        )
        +ProductMedia(
            position = 0
        )
    }
}

val template3A = Template(
    code = THREEA
) {
    Column {
        +Row {
            +Column(
                style = {
                    startPercentage = 0.3f
                    endPercentage = 0.9f
                }
            ) {
                +ProductMedia(
                    position = 0
                )
                +ProductInfo(
                    position = 0,
                    nameStyle = {
                        textSize = 14f
                    }
                )
            }
        }
        +Row {
            +Column(
                style = {
                    startPercentage = 0.1f
                    endPercentage = 0.45f
                }
            ) {
                +ProductMedia(
                    position = 1
                )
                +ProductExtendedInfo(
                    position = 1
                )
            }
            +Column(
                style = {
                    startPercentage = 0.55f
                    endPercentage = 0.9f
                }
            ) {
                +ProductMedia(
                    position = 2
                )
                +ProductExtendedInfo(
                    position = 2
                )
            }
        }
    }
}
