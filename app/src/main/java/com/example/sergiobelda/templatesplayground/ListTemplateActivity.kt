package com.example.sergiobelda.templatesplayground

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sergiobelda.templatesplayground.databinding.ActivityListTemplateBinding
import com.example.sergiobelda.templatesplayground.model.productBlock2
import com.example.sergiobelda.templatesplayground.model.productBlock3
import com.example.sergiobelda.templatesplayground.model.template1A
import com.example.sergiobelda.templatesplayground.model.template1B
import com.example.sergiobelda.templatesplayground.model.template2A
import com.example.sergiobelda.templatesplayground.model.template3A

class ListTemplateActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListTemplateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListTemplateBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.templateList.layoutManager = LinearLayoutManager(this)
        binding.templateList.adapter = TemplateListAdapter(
            arrayListOf(productBlock2, productBlock3),
            arrayListOf(template1A, template2A, template1B, template3A),
            this
        )

        /*
        val templateView1A = TemplateView(this).apply {
            setTemplate(template1A)
            setLifecycleOwner(this@ListTemplateActivity)
        }
        val templateView1B = TemplateView(this).apply {
            setTemplate(template1B)
            setLifecycleOwner(this@ListTemplateActivity)
        }
        binding.templateList.adapter = CustomViewListAdapter(
            arrayListOf(product, product2, product3),
            arrayListOf(templateView1A, templateView1B)
        )
        */
    }
}
