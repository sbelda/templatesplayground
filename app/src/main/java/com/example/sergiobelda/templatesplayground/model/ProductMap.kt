package com.example.sergiobelda.templatesplayground.model

@Deprecated("Tested with Jetpack Compose")
data class ProductMap(val map: Map<Int, Any>)

val productMap1 = ProductMap(
    map = hashMapOf(
        Pair(1, "Product 1"),
        Pair(2, "12.23€")
    )
)

/*
@Composable
fun mapTemplate(productMap: ProductMap, component: Component) {
    when (component) {
        is Component.Column -> androidx.compose.foundation.layout.Column(
            modifier = Modifier.padding(
                PaddingValues(
                    start = component.styles.paddingStart.dp,
                    end = component.styles.paddingEnd.dp,
                    top = component.styles.paddingTop.dp,
                    bottom = component.styles.paddingBottom.dp
                )
            )
        ) {
            component.children.forEach {
                mapTemplate(productMap, component = it)
            }
        }
        is Component.Row -> androidx.compose.foundation.layout.Row(
            modifier = Modifier.padding(
                PaddingValues(
                    start = component.styles.paddingStart.dp,
                    end = component.styles.paddingEnd.dp,
                    top = component.styles.paddingTop.dp,
                    bottom = component.styles.paddingBottom.dp
                )
            )
        ) {
            component.children.forEach {
                mapTemplate(productMap, component = it)
            }
        }
        is Component.Text -> androidx.compose.material.Text(
            text = productMap.map[component.id]?.toString() ?: "",
            modifier = Modifier.padding(
                PaddingValues(
                    start = component.textStyle.paddingStart.dp,
                    end = component.textStyle.paddingEnd.dp,
                    top = component.textStyle.paddingTop.dp,
                    bottom = component.textStyle.paddingBottom.dp
                )
            )
        )
    }
}
 */
