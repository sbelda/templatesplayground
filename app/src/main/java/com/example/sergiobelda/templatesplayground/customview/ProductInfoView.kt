package com.example.sergiobelda.templatesplayground.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import com.example.sergiobelda.templatesplayground.databinding.ProductInfoViewBinding
import com.example.sergiobelda.templatesplayground.model.Product
import com.example.sergiobelda.templatesplayground.model.TextStyle

class ProductInfoView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ProductView(context, attrs, defStyleAttr) {

    private val binding = ProductInfoViewBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )

    fun setProductNameStyle(style: TextStyle) {
        binding.productNameText.textSize = style.textSize
    }

    fun setProductPriceStyle(style: TextStyle) {
        binding.productPriceText.textSize = style.textSize
    }

    override fun setProduct(product: Product) {
        binding.product = product
    }
}
