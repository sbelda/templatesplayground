package com.example.sergiobelda.templatesplayground

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.example.sergiobelda.templatesplayground.model.ProductsBlock
import com.example.sergiobelda.templatesplayground.model.Template

class TemplateListAdapter(private val items: ArrayList<ProductsBlock>, val templates: List<Template>, val lifecycleOwner: LifecycleOwner) :
    RecyclerView.Adapter<TemplateListAdapter.ViewHolder>() {

    inner class ViewHolder(private val templateView: TemplateView) : RecyclerView.ViewHolder(templateView) {

        fun bind(productsBlock: ProductsBlock) {
            templates.firstOrNull { it.code == productsBlock.templateCode }?.let {
                templateView.setTemplate(it)
                templateView.setLifecycleOwner(lifecycleOwner)
                templateView.setProductBlock(productsBlock)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            TemplateView(
                parent.context
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items.getOrNull(position)?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int = items.size
}
