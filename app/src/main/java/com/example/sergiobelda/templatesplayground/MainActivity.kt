package com.example.sergiobelda.templatesplayground

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sergiobelda.templatesplayground.databinding.ActivityMainBinding
import com.example.sergiobelda.templatesplayground.model.template3A

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val t = template3A

        binding.recyclerViewButton.setOnClickListener {
            startActivity(Intent(this, ListTemplateActivity::class.java))
        }

        binding.composeButton.setOnClickListener {
            startActivity(Intent(this, ComposeActivity::class.java))
        }
    }
}
