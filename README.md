# Templates Playground

#### 1. We receive an structure defining the skeleton of our views (No data provided). 

This structure is defined by Columns, Rows, and specific ProductMediaView, ProductInfoView and ProductExtendedInfoView. 
Each component receive the product as parameter when call `setProduct` in `TemplateView`. Files `product_media_view.xml`, `product_info_view.xml` or `product_extended_info_view.xml` are DataBinding layouts. `setProduct` binds product in layout.

```kotlin
// View Structure Skeleton
val template2A = Template(
    code = TWOA
) {
    Column {
        +ProductMedia(
            position = 0
        )
        +ProductInfo(
            position = 0,
            nameStyle = {
                textSize = 14f
            },
            priceStyle = {
                textSize = 12f
            }
        )
        +ProductMedia(
            position = 1
        )
        +ProductInfo(
            position = 1,
            nameStyle = {
                textSize = 20f
            },
            priceStyle = {
                textSize = 16f
            }
        )
    }
}

val template3A = Template(
    code = THREEA
) {
    Column {
        +Row {
            +Column(
                style = {
                    startPercentage = 0.3f
                    endPercentage = 0.9f
                }
            ) {
                +ProductMedia(
                    position = 0
                )
                +ProductInfo(
                    position = 0,
                    nameStyle = {
                        textSize = 14f
                    }
                )
            }
        }
        +Row {
            +Column(
                style = {
                    startPercentage = 0.1f
                    endPercentage = 0.45f
                }
            ) {
                +ProductMedia(
                    position = 1
                )
                +ProductExtendedInfo(
                    position = 1
                )
            }
            +Column(
                style = {
                    startPercentage = 0.55f
                    endPercentage = 0.9f
                }
            ) {
                +ProductMedia(
                    position = 2
                )
                +ProductExtendedInfo(
                    position = 2
                )
            }
        }
    }
}
```

![](./screenshots/device-2021-03-22-111754.png =100)

![](./screenshots/device-2021-03-22-111833.png =100)

- Columns: Vertical LinearLayout
- Row: ConstraintLayout with Guidelines (WIP WRAP_CONTENT HEIGHT) 

#### 2. This should be stored on a CustomView waiting for Data to be loaded.

*Alternative I: Define our CustomView as a DataBinding view waiting to bind Data.*

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>
        
        <variable
            name="product"
            type="com.example.sergiobelda.templatesplayground.model.Product" />
    </data> 
```

- Each component added to the CustomView from our View Skeleton Structure (e.g. Text), would have a property from our Data object (e.g. Product name).
- Once Data is loaded we will set the object to CustomView and bind it to all views that consume a property of this object.

Pros:
- Use binding for representing information.

Cons:
- We should create specific type of data structure to define what property of data object they should use (e.g. for a Product name, we should create a ProductName type in View Skeleton Structure to indicate that this should be convert to a TextView with the product name property value).

*Alternative II: Use LiveData and IDs from Figma as Views IDs*

```kotlin
    fun setLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        product.observe(lifecycleOwner, {
            findViewById<TextView>(it.templateNameId)?.text = it.name
            findViewById<TextView>(it.templatePriceId)?.text = it.price
        })
    }
```

Pros:
- Not use specific type of data structure for each attribute (more flexibility, scalability, and maintainability)

Cons:
- We should pass the Lifecycle of our Activity or Fragment.
- We should define additional variables in our local Data object.

#### Questions:
- Dark Mode
- Analytics impressions
- Clickable elements
- Keep these custom views in memory
- Play videos
- Map<K, V> for Figma ID and it's value

